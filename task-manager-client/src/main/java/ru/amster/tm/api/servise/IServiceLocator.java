package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.endpoint.*;

public interface IServiceLocator {

    void setSession(Session session);

    @NotNull Session getSession();

    void setCommandService(ICommandService commandService);

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();
}