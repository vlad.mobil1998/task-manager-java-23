package ru.amster.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;
import sun.misc.BASE64Decoder;

import java.io.*;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-base64-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load base64 data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 LOAD]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);

        @NotNull final byte[] base64 = new BASE64Decoder().decodeBuffer(fileInputStream);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        adminUserEndpoint.load(webServiceLocator.getSession(), domain);
        fileInputStream.close();
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

}