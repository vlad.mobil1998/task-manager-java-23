package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class EmailUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-email";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user email";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE EMAIL]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();

        System.out.println("ENTER EMAIL");
        @Nullable final String email = TerminalUtil.nextLine();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.updateUserEmail(webServiceLocator.getSession(), email);
        System.out.println("[OK]");
    }

}