package ru.amster.tm.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataBinaryClearCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-bin-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove binary data";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BINARY CLEAR]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_BINARY);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

}