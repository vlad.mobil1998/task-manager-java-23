package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class LastNameUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-last-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE LAST NAME]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();

        System.out.println("ENTER LAST NAME");
        @Nullable final String lastName = TerminalUtil.nextLine();
        if (lastName == null || lastName.isEmpty()) throw new EmptyEmailException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.updateUserLastName(webServiceLocator.getSession(), lastName);
        System.out.println("[OK]");
    }

}