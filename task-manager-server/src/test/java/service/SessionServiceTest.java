package service;

import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.SessionRepository;
import ru.amster.tm.service.ServiceLocator;
import ru.amster.tm.service.SessionService;
import ru.amster.tm.util.HashUtil;

public class SessionServiceTest {

    private IServiceLocator serviceLocator;

    private ISessionRepository sessionRepository;

    private ISessionService sessionService;

    @Before
    public void init() {
        serviceLocator = new ServiceLocator();
        sessionRepository = new SessionRepository();
        sessionService = new SessionService(sessionRepository);
    }

    @Test
    @Category(UnitCategory.class)
    public void signTest() {
        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");
        Session session1 = session.clone();

        sessionService.sign(session);
        sessionService.sign(session1);
        Assert.assertEquals(session.getSignature(), session1.getSignature());

        session1.setId("46546asd651");
        sessionService.sign(session1);
        Assert.assertNotEquals(session.getSignature(), session1.getSignature());

        session1.setId("46546asd65");
        session1.setTimestamp(1561564215456465416l);
        sessionService.sign(session1);
        Assert.assertNotEquals(session.getSignature(), session1.getSignature());
    }

    @Test
    @Category(UnitCategory.class)
    public void validateTest() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setRole(Role.USER);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);

        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestExceptionNull() {
        sessionService.validate(null);
    }

    @Test(expected = EmptyUserException.class)
    @Category(UnitCategory.class)
    public void validateTestException() {
        sessionService.setServiceLocator(serviceLocator);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException1() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLocked(true);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException2() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLocked(true);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException3() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLocked(true);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setTimestamp(1561564215456465415l);
        session.setUserId("1213asda12e444");
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException4() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLocked(true);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setTimestamp(1561564215456465415l);
        session.setUserId("1213asda12e444");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException5() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setLocked(true);
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");

        sessionRepository.merge(session);
        sessionService.sign(session);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException6() {
        Session session = new Session();
        sessionService.validate(session, null);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException7() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setId("1213asda12e444");
        userService.add(user);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException8() {
        sessionService.setServiceLocator(serviceLocator);

        IUserService userService = serviceLocator.getUserService();
        User user = new User();
        user.setId("1213asda12e444");
        user.setRole(Role.ADMIN);
        userService.add(user);

        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");
        sessionService.validate(session, Role.USER);
    }

    @Test
    @Category(UnitCategory.class)
    public void checkDataAccessTest() {
        sessionService.setServiceLocator(serviceLocator);

        User user = new User();
        User user1 = new User();

        user.setLogin("test");
        String passwordHash = HashUtil.salt("test");
        user.setPasswordHash(passwordHash);

        user1.setLogin("demo");
        String passwordHash1 = HashUtil.salt("demo");
        user1.setPasswordHash(passwordHash1);

        serviceLocator.getUserService().add(user);
        serviceLocator.getUserService().add(user1);

        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        Assert.assertTrue(sessionService.checkDataAccess("demo", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", ""));
        Assert.assertFalse(sessionService.checkDataAccess(null, "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", null));
        Assert.assertFalse(sessionService.checkDataAccess("test1", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("test", "demo"));
    }

    @Test
    @Category(UnitCategory.class)
    public void openTest() {
        sessionService.setServiceLocator(serviceLocator);

        User user = new User();
        User user1 = new User();

        user.setLogin("test");
        String passwordHash = HashUtil.salt("test");
        user.setPasswordHash(passwordHash);

        user1.setLogin("demo");
        String passwordHash1 = HashUtil.salt("demo");
        user1.setPasswordHash(passwordHash1);

        serviceLocator.getUserService().add(user);
        serviceLocator.getUserService().add(user1);

        Session session;

        session = sessionService.open("test", "test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getTimestamp());
        session = sessionService.open("demo", "demo");
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getTimestamp());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void signOutByLoginTest() {
        sessionService.setServiceLocator(serviceLocator);

        User user = new User();

        user.setLogin("test");
        String passwordHash = HashUtil.salt("test");
        user.setPasswordHash(passwordHash);

        serviceLocator.getUserService().add(user);

        Session session;

        session = sessionService.open("test", "test");
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getTimestamp());

        sessionService.signOutByLogin("test");
        sessionService.validate(session);
    }

}