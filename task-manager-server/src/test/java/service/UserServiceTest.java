package service;

import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.IUserRepository;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.repository.UserRepository;
import ru.amster.tm.service.UserService;
import ru.amster.tm.util.HashUtil;

public class UserServiceTest {

    private IUserRepository userRepository;

    private IUserService userService;

    @Before
    public void init() {
        userRepository = new UserRepository();
        userService = new UserService(userRepository);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        User userTest;
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userTest = userService.findById(user.getId());
        Assert.assertEquals(user, userTest);
        Assert.assertNotEquals(user1, userTest);
        Assert.assertNotEquals(user2, userTest);

        userTest = userService.findById(user1.getId());
        Assert.assertEquals(user1, userTest);
        Assert.assertNotEquals(user, userTest);
        Assert.assertNotEquals(user2, userTest);

        userTest = userService.findById(user2.getId());
        Assert.assertEquals(user2, userTest);
        Assert.assertNotEquals(user, userTest);
        Assert.assertNotEquals(user1, userTest);
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findByIdTestExceptionEmpty() {
        userService.findById("");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findByIdTestExceptionNull() {
        userService.findById(null);
    }

    @Test @Category(UnitCategory.class)
    public void findByLoginTest() {
        User userTest;
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userTest = userService.findByLogin("log");
        Assert.assertEquals(user, userTest);
        Assert.assertNotEquals(user1, userTest);
        Assert.assertNotEquals(user2, userTest);

        userTest = userService.findByLogin("asd");
        Assert.assertEquals(user1, userTest);
        Assert.assertNotEquals(user, userTest);
        Assert.assertNotEquals(user2, userTest);

        userTest = userService.findByLogin("sad");
        Assert.assertEquals(user2, userTest);
        Assert.assertNotEquals(user, userTest);
        Assert.assertNotEquals(user1, userTest);
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void findByLoginTestExceptionEmpty() {
        userService.findByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void findByLoginTestExceptionNull() {
        userService.findByLogin(null);
    }

    @Test @Category(UnitCategory.class)
    public void removeByIdTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");
        Assert.assertEquals(3, userRepository.getEntity().size());

        userService.removeById(user.getId());
        Assert.assertEquals(2, userRepository.getEntity().size());

        userService.removeById(user1.getId());
        Assert.assertEquals(1, userRepository.getEntity().size());

        userService.removeById(user2.getId());
        Assert.assertEquals(0, userRepository.getEntity().size());

    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeByIdTestExceptionEmpty() {
        userService.removeById("");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeByIdTestExceptionNull() {
        userService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLoginTest() {
        userService.create("log", "daa", "a@a");
        userService.create("asd", "da21", "B@b");
        userService.create("sad", "wea", "Q@a");
        Assert.assertEquals(3, userRepository.getEntity().size());

        userService.removeByLogin("log");
        Assert.assertEquals(2, userRepository.getEntity().size());

        userService.removeByLogin("asd");
        Assert.assertEquals(1, userRepository.getEntity().size());

        userService.removeByLogin("sad");
        Assert.assertEquals(0, userRepository.getEntity().size());
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void removeByLoginTestExceptionEmpty() {
        userService.removeByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void removeByLoginTestExceptionNull() {
        userService.removeByLogin(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void updateEmailTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userService.updateEmail(user.getId(), "a@b");
        Assert.assertNotEquals("a@a", user.getEmail());
        Assert.assertEquals("B@b", user1.getEmail());
        Assert.assertEquals("Q@a", user2.getEmail());

        userService.updateEmail(user1.getId(), "df@b");
        Assert.assertNotEquals("B@b", user1.getEmail());
        Assert.assertEquals("Q@a", user2.getEmail());
        Assert.assertEquals("a@b", user.getEmail());

        userService.updateEmail(user2.getId(), "asd@b");
        Assert.assertNotEquals("Q@a", user2.getEmail());
        Assert.assertEquals("df@b", user1.getEmail());
        Assert.assertEquals("a@b", user.getEmail());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateFirstNameTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userService.updateFirstName(user.getId(), "asd");
        Assert.assertNotEquals(null, user.getFistName());
        Assert.assertEquals(null, user1.getFistName());
        Assert.assertEquals(null, user2.getFistName());

        userService.updateFirstName(user1.getId(), "ddd");
        Assert.assertNotEquals(null, user1.getFistName());
        Assert.assertEquals("asd", user.getFistName());
        Assert.assertEquals(null, user2.getFistName());

        userService.updateFirstName(user2.getId(), "qwe");
        Assert.assertNotEquals(null, user2.getFistName());
        Assert.assertEquals("ddd", user1.getFistName());
        Assert.assertEquals("asd", user.getFistName());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateLastNameTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userService.updateLastName(user.getId(), "asd");
        Assert.assertNotEquals(null, user.getLastName());
        Assert.assertEquals(null, user1.getLastName());
        Assert.assertEquals(null, user2.getLastName());

        userService.updateLastName(user1.getId(), "ddd");
        Assert.assertNotEquals(null, user1.getLastName());
        Assert.assertEquals("asd", user.getLastName());
        Assert.assertEquals(null, user2.getLastName());

        userService.updateLastName(user2.getId(), "qwe");
        Assert.assertNotEquals(null, user2.getLastName());
        Assert.assertEquals("ddd", user1.getLastName());
        Assert.assertEquals("asd", user.getLastName());


    }

    @Test
    @Category(UnitCategory.class)
    public void updateMiddleNameTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userService.updateMiddleName(user.getId(), "asd");
        Assert.assertNotEquals(null, user.getMiddleName());
        Assert.assertEquals(null, user1.getMiddleName());
        Assert.assertEquals(null, user2.getMiddleName());

        userService.updateMiddleName(user1.getId(), "ddd");
        Assert.assertNotEquals(null, user1.getMiddleName());
        Assert.assertEquals("asd", user.getMiddleName());
        Assert.assertEquals(null, user2.getMiddleName());

        userService.updateMiddleName(user2.getId(), "qwe");
        Assert.assertNotEquals("", user2.getMiddleName());
        Assert.assertEquals("ddd", user1.getMiddleName());
        Assert.assertEquals("asd", user.getMiddleName());
    }

    @Test
    @Category(UnitCategory.class)
    public void updatePasswordTest() {
        User user;
        User user1;
        User user2;

        user = userService.create("log", "daa", "a@a");
        user1 = userService.create("asd", "da21", "B@b");
        user2 = userService.create("sad", "wea", "Q@a");

        userService.updatePassword(user.getId(), "asd");
        Assert.assertNotEquals(HashUtil.salt("daa"), user.getPasswordHash());

        userService.updatePassword(user1.getId(), "ddd");
        Assert.assertNotEquals(HashUtil.salt("da21"), user1.getPasswordHash());

        userService.updatePassword(user2.getId(), "qwe");
        Assert.assertNotEquals(HashUtil.salt("wea"), user2.getPasswordHash());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockAndUnlockUserByLoginTest() {
        User user;
        user = userService.create("log", "daa", "a@a");

        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin("log");
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin("log");
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void lockUserByLoginTestExceptionEmpty() {
        userService.lockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void lockUserByLoginTestExceptionNull() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void unlockUserByLoginTestExceptionEmpty() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    @Category(UnitCategory.class)
    public void unlockUserByLoginTestExceptionNull() {
        userService.unlockUserByLogin(null);
    }

}