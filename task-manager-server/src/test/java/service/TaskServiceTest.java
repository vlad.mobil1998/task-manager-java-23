package service;

import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.service.TaskService;

import java.util.ArrayList;
import java.util.List;


public class TaskServiceTest {

    private ITaskRepository taskRepository;

    private ITaskService taskService;

    private List<Task> tasks = new ArrayList<>();

    @Before
    public void addTaskData() {
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
        Task task = new Task();
        Task task1 = new Task();
        Task task2 = new Task();
        Task task3 = new Task();

        task.setUserId("1");
        task.setDescription("1d");
        task.setName("1n");
        tasks.add(task);

        task1.setUserId("1");
        task1.setDescription("2d");
        task1.setName("2n");
        tasks.add(task1);

        task2.setUserId("2");
        task2.setDescription("1d");
        task2.setName("1n");
        tasks.add(task2);

        task3.setUserId("3");
        task3.setDescription("2d");
        task3.setName("2n");
        tasks.add(task3);
    }

    @Test
    @Category(UnitCategory.class)
    public void createTaskTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        Assert.assertEquals(1, taskRepository.getEntity().size());
        String userId1 = "1";
        String description1 = "2d";
        String name1 = "2n";
        taskService.create(userId1, name1, description1);
        Assert.assertEquals(2, taskRepository.getEntity().size());
        String userId2 = "2";
        String description2 = "1d";
        String name2 = "1n";
        taskService.create(userId2, name2, description2);
        Assert.assertEquals(3, taskRepository.getEntity().size());
        String userId3 = "3";
        String description3 = "2d";
        String name3 = "2n";
        taskService.create(userId3, name3, description3);
        Assert.assertEquals(4, taskRepository.getEntity().size());
        taskService.create(userId, name, "");
        Assert.assertEquals(4, taskRepository.getEntity().size());
        taskService.create(userId, name, null);
        Assert.assertEquals(4, taskRepository.getEntity().size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionEmpty() {
        taskService.create("", "ds", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionNull() {
        taskService.create(null, "sd", "ad");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionEmpty1() {
        taskService.create("1", "", "ds");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createTaskTestExceptionNull1() {
        taskService.create("1", null, "sds");
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        Task Task;
        Task = taskService.findOneByName("1", "1n");
        Assert.assertEquals(userId, Task.getUserId());
        Assert.assertEquals(description, Task.getDescription());
        Assert.assertEquals(name, Task.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task task;
        task = taskService.findOneByName("1", "1n");
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(description, task.getDescription());
        Assert.assertEquals(name, task.getName());
        task = taskService.findOneByName("2", "1n");
        Assert.assertEquals(userId1, task.getUserId());
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.findOneByName("2", "1n");
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.findOneByName("1", "1");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty() {
        taskService.findOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull() {
        taskService.findOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty1() {
        taskService.findOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull1() {
        taskService.findOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task task;
        Task task1;
        task = taskService.findOneByName("1", "1n");
        task1 = taskService.findOneById("1", task.getId());
        Assert.assertEquals(task, task1);
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.findOneById("1", "213");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty2() {
        taskService.findOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull1() {
        taskService.findOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty() {
        taskService.findOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull() {
        taskService.findOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "1";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.findOneByIndex("1", 0);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.findOneByIndex("1", -1);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.findOneByIndex("1", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionEmpty() {
        taskService.findOneByIndex("", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionNull() {
        taskService.findOneByIndex(null, 1);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionNull1() {
        taskService.findOneByIndex("as", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "1";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.removeOneByIndex("1", 0);
        Assert.assertEquals(1, taskRepository.getEntity().size());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.removeOneByIndex("1", -1);
        Assert.assertEquals(2, taskRepository.getEntity().size());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        taskService.removeOneByIndex("1", 3);
        Assert.assertEquals(2, taskRepository.getEntity().size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionEmpty() {
        taskService.removeOneByIndex("", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionNull() {
        taskService.removeOneByIndex(null, 3);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionNull1() {
        taskService.removeOneByIndex("asd", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task Task;
        Task Task1;
        Task = taskService.findOneByName("1", "1n");
        Task1 = taskService.removeOneById("1", Task.getId());
        Assert.assertEquals(Task, Task1);
        Assert.assertNotEquals(2, taskRepository.getEntity().size());
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.removeOneById("1", "213");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty2() {
        taskService.removeOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull1() {
        taskService.removeOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty() {
        taskService.removeOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull() {
        taskService.removeOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        Task Task;
        Task = taskService.removeOneByName("1", "1n");
        Assert.assertEquals(userId, Task.getUserId());
        Assert.assertEquals(description, Task.getDescription());
        Assert.assertEquals(name, Task.getName());
        Assert.assertEquals(0, taskRepository.getEntity().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task Task;
        Task = taskService.removeOneByName("1", "1n");
        Assert.assertEquals(userId, Task.getUserId());
        Assert.assertEquals(description, Task.getDescription());
        Assert.assertEquals(name, Task.getName());
        Assert.assertEquals(1, taskRepository.getEntity().size());
        Task = taskService.removeOneByName("2", "1n");
        Assert.assertEquals(userId1, Task.getUserId());
        Assert.assertEquals(0, taskRepository.getEntity().size());
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.removeOneByName("2", "1n");
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.removeOneByName("1", "1");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty() {
        taskService.removeOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull() {
        taskService.removeOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty1() {
        taskService.removeOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull1() {
        taskService.removeOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void updateTaskByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task Task;
        Task = taskService.updateTaskByIndex("1", 0, "2name", "2description");
        Assert.assertEquals(userId, Task.getUserId());
        Assert.assertNotEquals(description, Task.getDescription());
        Assert.assertNotEquals(name, Task.getName());
        Task = taskService.updateTaskByIndex("2", 1, "1name", "1description");
        Assert.assertEquals(userId1, Task.getUserId());
        Assert.assertNotEquals(description1, Task.getDescription());
        Assert.assertNotEquals(name1, Task.getName());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.updateTaskByIndex("2", 1, "asd", "erwq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIndexTestExceptionEmpty() {
        taskService.updateTaskByIndex("", 1, "asd", "asd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIndexTestExceptionNull() {
        taskService.updateTaskByIndex(null, 1, "ds", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIndexTestExceptionNull2() {
        taskService.updateTaskByIndex("ds", 1, null, "ds");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIndexTestExceptionEmpty1() {
        taskService.updateTaskByIndex("ds", 1, "", "ds");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateTaskByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        taskService.create(userId1, name1, description1);
        Task Task;
        Task Task1;
        Task = taskService.findOneByName("1", "1n");
        Task1 = taskService.updateTaskById("1", Task.getId(), "1name", "1description");
        Assert.assertEquals(userId, Task1.getUserId());
        Assert.assertNotEquals(description, Task1.getDescription());
        Assert.assertNotEquals(name, Task1.getName());
        Task = taskService.findOneByName("2", "1n");
        Assert.assertEquals(userId1, Task.getUserId());
        Assert.assertEquals(name1, Task.getName());
        Assert.assertEquals(description1, Task.getDescription());
    }

    @Test(expected = EmptyTaskException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        taskService.create(userId, name, description);
        taskService.updateTaskById("1", "213", "ds", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty() {
        taskService.updateTaskById("", "sd", "dsa", "saq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull1() {
        taskService.updateTaskById(null, "ds", "we", "re");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty1() {
        taskService.updateTaskById("sds", "", "we", "qwe");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull() {
        taskService.updateTaskById("ds", null, "esa", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionEmpty2() {
        taskService.updateTaskById("sds", "sd", "", "qwe");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateTaskByIdTestExceptionNull2() {
        taskService.updateTaskById("ds", "sda", null, "asd");
    }

}
