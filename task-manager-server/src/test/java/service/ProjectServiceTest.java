package service;


import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {
    
    private IProjectRepository projectRepository;

    private IProjectService projectService;

    private List<Project> projects = new ArrayList<>();

    @Before
    public void addProjectData() {
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        Project project = new Project();
        Project project1 = new Project();
        Project project2 = new Project();
        Project project3 = new Project();

        project.setUserId("1");
        project.setDescription("1d");
        project.setName("1n");
        projects.add(project);

        project1.setUserId("1");
        project1.setDescription("2d");
        project1.setName("2n");
        projects.add(project1);

        project2.setUserId("2");
        project2.setDescription("1d");
        project2.setName("1n");
        projects.add(project2);

        project3.setUserId("3");
        project3.setDescription("2d");
        project3.setName("2n");
        projects.add(project3);
    }

    @Test
    @Category(UnitCategory.class)
    public void createProjectTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        Assert.assertEquals(1, projectRepository.getEntity().size());
        String userId1 = "1";
        String description1 = "2d";
        String name1 = "2n";
        projectService.create(userId1, name1, description1);
        Assert.assertEquals(2, projectRepository.getEntity().size());
        String userId2 = "2";
        String description2 = "1d";
        String name2 = "1n";
        projectService.create(userId2, name2, description2);
        Assert.assertEquals(3, projectRepository.getEntity().size());
        String userId3 = "3";
        String description3 = "2d";
        String name3 = "2n";
        projectService.create(userId3, name3, description3);
        Assert.assertEquals(4, projectRepository.getEntity().size());
        projectService.create(userId, name, "");
        Assert.assertEquals(4, projectRepository.getEntity().size());
        projectService.create(userId, name, null);
        Assert.assertEquals(4, projectRepository.getEntity().size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionEmpty() {
        projectService.create("", "ds", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionNull() {
        projectService.create(null, "sd", "ad");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionEmpty1() {
        projectService.create("1", "", "ds");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void createProjectTestExceptionNull1() {
        projectService.create("1", null, "sds");
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        Project project;
        project = projectService.findOneByName("1", "1n");
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertEquals(name, project.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        project = projectService.findOneByName("1", "1n");
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertEquals(name, project.getName());
        project = projectService.findOneByName("2", "1n");
        Assert.assertEquals(userId1, project.getUserId());
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.findOneByName("2", "1n");
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.findOneByName("1", "1");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty() {
        projectService.findOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull() {
        projectService.findOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionEmpty1() {
        projectService.findOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void findOneByNameTestExceptionNull1() {
        projectService.findOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        Project project1;
        project = projectService.findOneByName("1", "1n");
        project1 = projectService.findOneById("1", project.getId());
        Assert.assertEquals(project, project1);
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.findOneById("1", "213");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty2() {
        projectService.findOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull1() {
        projectService.findOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionEmpty() {
        projectService.findOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void findOneByIdTestExceptionNull() {
        projectService.findOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "1";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.findOneByIndex("1", 0);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.findOneByIndex("1", -1);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.findOneByIndex("1", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionEmpty() {
        projectService.findOneByIndex("", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionNull() {
        projectService.findOneByIndex(null, 1);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void findOneByIndexTestExceptionNull1() {
        projectService.findOneByIndex("as", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "1";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.removeOneByIndex("1", 0);
        Assert.assertEquals(1, projectRepository.getEntity().size());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.removeOneByIndex("1", -1);
        Assert.assertEquals(2, projectRepository.getEntity().size());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        projectService.removeOneByIndex("1", 3);
        Assert.assertEquals(2, projectRepository.getEntity().size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionEmpty() {
        projectService.removeOneByIndex("", 3);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionNull() {
        projectService.removeOneByIndex(null, 3);
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void removeOneByIndexTestExceptionNull1() {
        projectService.removeOneByIndex("asd", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        Project project1;
        project = projectService.findOneByName("1", "1n");
        project1 = projectService.removeOneById("1", project.getId());
        Assert.assertEquals(project, project1);
        Assert.assertNotEquals(2, projectRepository.getEntity().size());
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.removeOneById("1", "213");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty2() {
        projectService.removeOneById("", "sd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull1() {
        projectService.removeOneById(null, "ds");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionEmpty() {
        projectService.removeOneById("sds", "");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void removeOneByIdTestExceptionNull() {
        projectService.removeOneById("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        Project project;
        project = projectService.removeOneByName("1", "1n");
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(0, projectRepository.getEntity().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        project = projectService.removeOneByName("1", "1n");
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(1, projectRepository.getEntity().size());
        project = projectService.removeOneByName("2", "1n");
        Assert.assertEquals(userId1, project.getUserId());
        Assert.assertEquals(0, projectRepository.getEntity().size());
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.removeOneByName("2", "1n");
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestException1() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.removeOneByName("1", "1");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty() {
        projectService.removeOneByName("", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull() {
        projectService.removeOneByName(null, "sd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionEmpty1() {
        projectService.removeOneByName("sd", "");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void removeOneByNameTestExceptionNull1() {
        projectService.removeOneByName("ds", null);
    }

    @Test
    @Category(UnitCategory.class)
    public void updateProjectByIndexTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        project = projectService.updateProjectByIndex("1", 0, "2name", "2description");
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertNotEquals(description, project.getDescription());
        Assert.assertNotEquals(name, project.getName());
        project = projectService.updateProjectByIndex("2", 1, "1name", "1description");
        Assert.assertEquals(userId1, project.getUserId());
        Assert.assertNotEquals(description1, project.getDescription());
        Assert.assertNotEquals(name1, project.getName());
    }

    @Test(expected = InvalidIndexException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIndexTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.updateProjectByIndex("2", 1, "asd", "erwq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIndexTestExceptionEmpty() {
        projectService.updateProjectByIndex("", 1, "asd", "asd");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIndexTestExceptionNull() {
        projectService.updateProjectByIndex(null, 1, "ds", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIndexTestExceptionNull2() {
        projectService.updateProjectByIndex("ds", 1, null, "ds");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIndexTestExceptionEmpty1() {
        projectService.updateProjectByIndex("ds", 1, "", "ds");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateProjectByIdTest() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        String userId1 = "2";
        String description1 = "1d";
        String name1 = "1n";
        projectService.create(userId1, name1, description1);
        Project project;
        Project project1;
        project = projectService.findOneByName("1", "1n");
        project1 = projectService.updateProjectById("1", project.getId(), "1name", "1description");
        Assert.assertEquals(userId, project1.getUserId());
        Assert.assertNotEquals(description, project1.getDescription());
        Assert.assertNotEquals(name, project1.getName());
        project = projectService.findOneByName("2", "1n");
        Assert.assertEquals(userId1, project.getUserId());
        Assert.assertEquals(name1, project.getName());
        Assert.assertEquals(description1, project.getDescription());
    }

    @Test(expected = EmptyProjectException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestException() {
        String userId = "1";
        String description = "1d";
        String name = "1n";
        projectService.create(userId, name, description);
        projectService.updateProjectById("1", "213", "ds", "ds");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty() {
        projectService.updateProjectById("", "sd", "dsa", "saq");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull1() {
        projectService.updateProjectById(null, "ds", "we", "re");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty1() {
        projectService.updateProjectById("sds", "", "we", "qwe");
    }

    @Test(expected = EmptyIdException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull() {
        projectService.updateProjectById("ds", null, "esa", "asd");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionEmpty2() {
        projectService.updateProjectById("sds", "sd", "", "qwe");
    }

    @Test(expected = EmptyNameException.class)
    @Category(UnitCategory.class)
    public void updateProjectByIdTestExceptionNull2() {
        projectService.updateProjectById("ds", "sda", null, "asd");
    }

}