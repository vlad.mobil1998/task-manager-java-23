package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;

public class EndpointTest {

    private IServiceLocator serviceLocator;

    private SessionEndpoint sessionEndpoint;

    private ProjectEndpoint projectEndpoint;

    private UserEndpoint userEndpoint;

    private Session sessionTest;

    private Session sessionTest1;

    private User user;

    private User user1;

    @Before
    public void init() {
        serviceLocator = new ServiceLocator();
        sessionEndpoint = new SessionEndpoint(serviceLocator);
        projectEndpoint = new ProjectEndpoint(serviceLocator);
        userEndpoint = new UserEndpoint(serviceLocator);
        IUserService userService = serviceLocator.getUserService();
        user = userService.create("test", "test", "t@t.ru");
        user1 = userService.create("admin", "admin", Role.ADMIN);
        sessionTest = sessionEndpoint.openSession("test", "test");
        sessionTest1 = sessionEndpoint.openSession("admin", "admin");
    }

    @Test
    public void login() {
        Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertEquals(session.getUserId(), user.getId());
        projectEndpoint.createWithTwoParamProject(session, "test");
        session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertEquals(session.getUserId(), user1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void logout() {
        sessionEndpoint.closeSession(sessionTest);
        projectEndpoint.findProjectByName(sessionTest, "test");
    }

    @Test(expected = EmptyProjectException.class)
    public void projectCreate() {
        projectEndpoint.createWithTwoParamProject(sessionTest1, "demo");
        Project project = serviceLocator.getProjectService().findOneByName(sessionTest1.getUserId(),"demo");
        Assert.assertEquals("demo", project.getName());
        Assert.assertEquals(sessionTest1.getUserId(), project.getUserId());
        sessionTest = sessionEndpoint.openSession("test", "test");
        serviceLocator.getProjectService().findOneByName(sessionTest.getUserId(),"demo");
    }


}
